FROM node:8.12
MAINTAINER ARS <arsit@aislerocket.com>

ADD . /app
WORKDIR /app
RUN yarn install

EXPOSE 3000

CMD ["yarn"]

